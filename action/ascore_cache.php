<?php
if (!defined("_ECRIRE_INC_VERSION")) return;
#---------------------------------------------------#
#  Plugin  : AScore                                 #
#  Auteur  : Patrice Vanneufville, 2021             #
#  Contact : patrice¡.!vanneufville¡@!laposte¡.!net #
#  Licence : GPL                                    #
#--------------------------------------------------------------------------#
#  Documentation : https://contrib.spip.net/Des-partitions-de-musique      #
#--------------------------------------------------------------------------#

function action_ascore_cache_dist() {

	// $securiser_action = charger_fonction('securiser_action', 'inc');
	// $arg = $securiser_action();
	$source = _request('source');
	if(!$source) return;

	$dir = sous_repertoire(_DIR_CACHE, 'ascore');
	$cmd = _request('cmd');
	switch($cmd) {
    case 'putASCORE':
			if($image = _request('image')) {
				$inline = base64_decode(_request('inline'));
				$format = _request('format');
				$inline = "\n<!--\n " . date('Y-m-d H:i:s') . ' - Len ' .strlen($inline) . ' - Format ' . strtoupper($format) 
					. "\n-->\n<!-- " . ascore_truncate($inline, 800) . "\n-->\n";
				ecrire_fichier($dir . $source . '.html', $inline . base64_decode($image));
			}
			return;
    case 'putPNG':
			if($data = _request('data')) {
				$data = explode(';base64,', $data);
				ecrire_fichier($dir . $source . '.png', base64_decode($data[1]));
			}
			return;
    case 'putJPG':
    case 'putJPEG':
			if($data = _request('data')) {
				$data = explode(';base64,', $data);
				ecrire_fichier($dir . $source . '.jpg', base64_decode($data[1]));
			}
    case 'getASCORE':
			if($source = _request('source')) {
				if(lire_fichier($dir . $source . '.html', $c))
					echo unicode2charset(charset2unicode($c,'iso-8859-1'));
			}
			return;
	}
}

function ascore_truncate($string, $length, $dots = " ...") {
    return (strlen($string) > $length) ? substr($string, 0, $length - strlen($dots)) . $dots : $string;
}

?>