<?php
#ini_set('display_errors','1'); error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
if (!defined("_ECRIRE_INC_VERSION")) return;
#---------------------------------------------------#
#  Plugin  : AScore                                 #
#  Auteur  : Patrice Vanneufville, 2021             #
#  Contact : patrice¡.!vanneufville¡@!laposte¡.!net #
#  Licence : GPL                                    #
#--------------------------------------------------------------------------#
#  Documentation : https://contrib.spip.net/Des-partitions-de-musique      #
#--------------------------------------------------------------------------#

// filtre qui retire le code source des partitions du texte original
function pas_de_balise_ascore($texte) {
	if(strpos($texte, '<') === false)
    return $texte;
	return preg_replace(',<(' . _SCORE_REGEXP . ')>.*?</(\\1)>,UimsS', '', $texte);
}

// filtre qui renvoie tous les aliases de configuration ($key en minuscules)
function ascore_aliases($key = "", $flip = false) {
	static $aliases = array(
		'largeur' => 'xLargeur', 'class' => 'xClass',       'style' => 'xStyle',
		'balise' => 'xBalise',  'marges' => 'xMarges',      'zoom'  => 'xZoom',
		'align' => 'xAlign',      'code' => 'xCode',        'cache' => 'xCache',
		'mini' => 'xMini',      'inline' => 'xInline', 'styletitre' => 'xTitreCSS'
		    
	);
	static $aliases_flip;
	if(!$aliases_flip)
    $aliases_flip = array_flip($aliases);
	if(!$key)
		return json_encode($flip ? $aliases_flip : $aliases);
	$lkey = strtolower($key);
	if($flip)
		return isset($aliases_flip[$key]) ? $aliases_flip[$key] : $key;
	return isset($aliases[$lkey]) ? $aliases[$lkey] : $key;
}

// aide le Couteau Suisse a calculer la balise #INTRODUCTION
$GLOBALS['cs_introduire'][] = 'pas_de_balise_ascore';
// aide le Couteau Suisse a echapper les balises des partitions lors des traitements typo
$GLOBALS['cs_echapper'][] = _SCORE_REGEXP;

?>
