<?php
if (!defined("_ECRIRE_INC_VERSION")) return;
#---------------------------------------------------#
#  Plugin  : AScore                                 #
#  Auteur  : Patrice Vanneufville, 2021             #
#  Contact : patrice¡.!vanneufville¡@!laposte¡.!net #
#  Licence : GPL                                    #
#--------------------------------------------------------------------------#
#  Documentation : https://contrib.spip.net/Des-partitions-de-musique      #
#--------------------------------------------------------------------------#

// balises du plugin a inserer dans les articles
define('_SCORE_DEBUT', '<ascore>');
define('_SCORE_FIN', '</ascore>');
define('_SCORE_ABC_DEBUT', '<abc>');
define('_SCORE_ABC_FIN', '</abc>');
define('_SCORE_PAE_DEBUT', '<pae>');
define('_SCORE_PAE_FIN', '</pae>');
define('_SCORE_REGEXP', 'ascore|abc|pae');

define('_VEROVIO_HEAD', "<!-- HEAD VEROVIO -->");

?>