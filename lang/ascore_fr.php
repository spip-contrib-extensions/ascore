<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/ascore.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'donnees_alterees' => 'Données altérées. Il est nécessaire d’utiliser la syntaxe suivante : @syntaxe@.',

	// E
	'erreur_doc_introuvable' => 'Le document n°@element@ est introuvable !',
	'erreur_inline_introuvable' => 'Insert n°@element@ : aucune donnée musicale trouvée !',
	'erreur_log' => 'Log : @element@',
	'erreur_option_inconnue' => 'L’option de configuration "@element@" est inconnue !',

	// I
	'ignore_zoom' => 'L’option "@oups@" définit déjà l’échelle. L’option "@zoom@" est ignorée.',

	// V
	'vos_donnees' => 'Vos données...',

	// Z
	'zone_partition' => 'Un extrait musical est en construction ici. Recalculer la page et/ou vérifier le bon déroulement des fonctions Javascript.'
);
