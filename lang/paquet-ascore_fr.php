<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/ascore.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ascore_description' => 'Des partitions avec SPIP !

Voici pour vous la possibilité d’insérer dans vos contenus, des extraits musicaux grâce à la librairie {{[Verovio->https://www.verovio.org]}}.

L’idée est de créer des images au format SVG à la volée, en Javascript, dès le chargement de la page.

Utilisation :
-* Les données figurent en clair entre les balises <code><ascore></code> et <code></ascore></code>, <code><abc></code> et <code></abc></code>, ainsi que <code><pae></code> et <code></pae></code>.
-* Les documents sont accessibles grâce au modèle <code><scoreXX></code> où XX est l’identifiant du document.

Format des données : ABC, Plaine and Easie (PAE), MusicXML, MEI, Humdrum et midi (exportation).
_ Explications : https://book.verovio.org/toolkit-reference/input-formats.html
',
	'ascore_nom' => 'Partitions',
	'ascore_slogan' => 'Créez facilement des partitions dans vos textes.'
);
