<?php

#---------------------------------------------------#
#  Plugin  : AScore                                 #
#  Auteur  : Patrice Vanneufville, 2021             #
#  Contact : patrice¡.!vanneufville¡@!laposte¡.!net #
#  Licence : GPL                                    #
#--------------------------------------------------------------------------#
#  Documentation : https://contrib.spip.net/Des-partitions-de-musique      #
#--------------------------------------------------------------------------#

if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

function ascore_upgrade($nom_meta_base_version, $version_cible) {
	$maj = array();

	$maj['create'] = array(
		/* Ajouter les nouvelles extensions en base pour qu'elles soient telechargeables */
		array('sql_insertq_multi', 'spip_types_documents', array(
			array('extension' => 'mei', 'titre' => 'Music Encoding Initiative',   'inclus' => 'non', 'upload' => 'oui', 'mime_type' => 'application/vnd.mei+xml', 'media_defaut' => 'file'),
			array('extension' => 'abc', 'titre' => 'ABC Music Notation',   'inclus' => 'non', 'upload' => 'oui', 'mime_type' => 'text/vnd.abc', 'media_defaut' => 'file'),
			
		)),
	);
	$maj['0.1'] = array(
		array('sql_insertq', 'spip_types_documents', array('extension' => 'krn', 'titre' => 'Humdrum Music Notation', 'inclus' => 'non', 'upload' => 'oui', 'mime_type' => 'text/x-humdrum', 'media_defaut' => 'file')),
	);
	$maj['0.2'] = array(
		array('sql_insertq', 'spip_types_documents', array('extension' => 'musicxml', 'titre' => 'MusicXML', 'inclus' => 'non', 'upload' => 'oui', 'mime_type' => 'application/vnd.recordare.musicxml+xml', 'media_defaut' => 'file')),
	);

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}


function ascore_vider_tables($nom_meta_base_version) {
	sql_delete("spip_types_documents", sql_in("extension", array('mei', 'abc', 'krn', 'musicxml')));

	effacer_meta($nom_meta_base_version);
}

?>