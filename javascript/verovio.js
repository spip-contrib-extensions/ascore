/* Number of the first score */
var verovioId = 0;
var verovioTools = [];
var verovioRenders = [];

// documentation JSON keys : https://book.verovio.org/toolkit-reference/toolkit-options.html
var defaultOptions = {
	//justifyVertically : false,    // Justify spacing vertically to fill the page
	//pageMarginTop : 0,            // The page top margin (default: 50; min: 0; max: 500)
	//pageMarginBottom : 0,         // The page bottom margin (default: 50; min: 0; max: 500)
	pageWidth : 1700,        // The page width (default: 2100; min: 100; max: 60000)
	font: 'Bravura',         // Set the music font (default: "Leipzig") : https://book.verovio.org/advanced-topics/smufl.html
	svgViewBox: true,        // Use viewBox on svg root element for easy scaling of document
	breaks: 'auto',          // Define page and system breaks layout (default: "auto"; other values: 'none', 'auto', 'line', 'smart', 'encoded')
	adjustPageHeight: true,  // Adjust the page height to the height of the content
}

function verovioError(id, element, attrId) {
	if(!element || !ascore_errors) return;
	var msg = ascore_errors[id].replace("@element@", element);
	if(attrId)
    document.getElementById(attrId).innerHTML = msg;
  else
    return '<div class="ascore_msg">' + msg  + '</div>';
}

function verovioCreate(from) {
	/* Create the #outputNN div */
	var options  = Object.assign({}, defaultOptions, $(from).data('options2'));//, $(from).data('options4'));
	var xOptions = Object.assign({}, $(from).data('options1'), $(from).data('options3'));
	var file     = $(from).data('file') || '';
	var inline   = $(from).data('inline') || '';
	var id = Math.round(+new Date()).toString() + '-' + ++verovioId;
	var attrId = 'verovioOutput' + id;
	var errId = 'verovioError' + id;
	var fromStyle = xOptions.xStyle ? xOptions.xStyle : '';
	var fromClass = 'ascore ascore-' + verovioId;
	var svgClass = 'ascore_svg ascore_svg-' + verovioId;
	var msgClass = 'ascore_msg ascore_msg-' + verovioId;
	if(options.inputFrom) {
		fromClass += ' ascore_' + options.inputFrom;
		svgClass += ' ascore_svg_' + options.inputFrom;
		msgClass += ' ascore_msg_' + options.inputFrom;
	}
	if(!xOptions.xClass) xOptions.xClass = '';
	if(xOptions.xMini && Number.isInteger(xOptions.xMini)) {
		options.svgViewBox = false;
		options.footer = 'none';
		options.header = 'none';
		options.pageMarginTop = 30;
		options.pageMarginBottom = 30;
		options.pageMarginLeft = 30;
		options.pageMarginRight = 30;
		//options.pageWidth = 2970;
		options.scale = xOptions.xMini; // * xOptions.xMini / options.pageWidth;
		//options.shrinkToFit = true;
		xOptions.xClass += ' ' + 'ascore_mini';
	}
	if(xOptions.xClass) {
		fromClass += ' ' + xOptions.xClass;
		svgClass += ' ' + xOptions.xClass;
		msgClass += ' ' + xOptions.xClass;
	}
	if(xOptions.xAlign) {
		switch(xOptions.xAlign) {
			case 'center': case 'centre':
				fromClass += ' ascore_center';
				break;
			case 'right': case 'droite':
				fromClass += ' ascore_right';
				break;
			case 'left': case 'gauche':
				fromClass += ' ascore_left';
				break;
			default:
		}
	}
	if(xOptions.xLargeur && Number.isInteger(xOptions.xLargeur))
		options.pageWidth = xOptions.xLargeur;
	options  = Object.assign({}, options, $(from).data('options4'));
	$(from).removeAttr("data-options1 data-options2 data-options3 data-options4");
	$(from).html('');
	$(from).attr({"data-options": JSON.stringify(options), "data-xoptions": JSON.stringify(xOptions)});
	$(from).attr({class: fromClass, style: fromStyle});
	$(from).append(jQuery(document.createElement(xOptions.xBalise)).attr({id: attrId, class: svgClass}));
	$(from).append(jQuery(document.createElement('div')).attr({id: errId, class: msgClass}));
	var inputFormat = options.inputFrom ? options.inputFrom : '';
	/* Create the Vevorio toolkit instance */
	verovioTools[id] = new verovio.toolkit();
	/* Liste des options internes */
	var listOptions = Object.keys(verovioTools[id].getOptions());
	/* D'abord les options de creation */
	listOptions.push('inputFrom', 'scale', 'xmlIdSeed');
	Object.keys(options).reduce(function (err, val) {
		if(listOptions.indexOf(val) === -1) err.push(val);
		return err;
	}, []).forEach(element => $(from).append(verovioError(0, element)));
	/* Ensuite les datas musicales */
	if(inline) {
		inline = atob(inline);
		verovioRenders[id] = function(){ 
			console.log('Création inline. ' + attrId);
			/* Render the data and insert it as content of the #outputNN div */
			document.getElementById(attrId).innerHTML = verovioTools[id].renderData(inline, options);
			/* if(xOptions.xDebug) */
				 verovioError(2, verovioTools[id].getLog(), errId);
		}
	} else if(file) {
		verovioRenders[id] = function(){ 
			console.log('Création Fichier. ' + attrId);
			fetch(file)
				.then( function(response) { return response.text(); } )
				.then( function(meiXML) {
					inline = meiXML;
					var svg = verovioTools[id].renderData(meiXML, options);
					document.getElementById(attrId).innerHTML = svg;
					verovioError(2, verovioTools[id].getLog(), errId);
				});
		}
	} else {
		$(from).append(verovioError(3, verovioId));
		return;
	}
	/* Affiche l'image SVG */
	verovioRenders[id]();
	if(inputFormat == 'abc') {
		// possibilite de decaler horizontaleement les notations du dessus de la portee
		$('tspan.text').filterDecal().each(function() {
			var html = $(this).html();
			var match = html.match(/^\-?[\d\.]+(?:%|px)/);
			if(match) {
				$(this).html(html.replace(match[0], ''));
				$(this).parents('g.harm').css('transform', 'translateX(' + match[0] + ')');
			}
		});
		// gestion artificielle des triolets non disponibles actuellement. Exemple : A"3"BC
		$('tspan.text').not('.triolet').filter(function(){ return $(this).html() === '3';})
			.attr('font-style', 'italic').each(function() {
				var s = $(this).attr('font-size');
				if(!s) return;
				s = s.replace(/^(\d+)(.*)$/, function(match, a, b) { return String(Math.round(a*0.75)) + b; });
				$(this).attr({'font-size': s, 'font-weight':'bold'}).addClass('triolet');
			console.log(s);
			});
	}
	$('#' + attrId + '.sansBarreDeFin g.barLineAttr').last().hide();
	$('#' + attrId + ' g.pgHead tspan.text').css(JSON.parse('{' + xOptions.xTitreCSS + '}'))
	/* Affiche le code si c'est l'option est activee */
	if(xOptions.xInline) {
		$('#' + attrId + ' .definition-scale').attr('color', 'currentColor');
		$('#' + attrId + ' path').each(function() {
			if(!this.fill) $(this).attr('fill', 'currentColor');
		});
	}
	// css(JSON.parse('{' + xOptions.xTitreCSS + '}'))
	if(xOptions.xCode) {
		if(xOptions.xCode == 'debug') 
			xOptions.xCode = 'source abc pae mei svg png jpg midi';
		xOptions.xCode.split(/[^\w]+/).unique().forEach(function(val){
			var data = '', append = '';
			val = val.toLowerCase();
			var titre = $('.pgHead', from).eq(0).text().trim();
			var alt = titre ? titre : 'Une partition';
			switch(val) {
				case 'source':
					data = inline;
					break;
				case 'pae':
					data = verovioTools[id].renderToPAE();
					break;
				case 'abc':
					if(inputFormat == val) {
						data = inline; 
						val = 'source'; 
					}
					break;
				case 'mei':
					data = verovioTools[id].getMEI();
					break;
				case 'midi':
					var base64 = verovioTools[id].renderToMIDI();
					data = navigator.requestMIDIAccess
							? 'WebMIDI est disponible sur ce navigateur ! À suivre donc...'
							: "WebMIDI n'est pas disponible sur ce navigateur.\nVeuillez utiliser Chrome, Edge, Opera ou Samsung Internet.";
					titre = titre ? '[' + titre + ']' : 'le morceau';
					append = '<div class="ascore_download">=> <a download="file.midi' + '" href="data:audio/midi;base64,' 
						+ base64 + '">Télécharger ' + titre + ' en MIDI</a></div>';
					break;
				case 'jpg':
				case 'jpeg':
				case 'png':
				/* case 'gif': // idem png en fait */
					var img = new Image;
					$(img).attr({title: alt, alt: alt, id: val + id});
					var telecharger = $(document.createElement('a')).attr({href: "#", download: alt }).text('Télécharger')
					var whenLoaded = function(img) { telecharger.attr('href', img.src);	}
					SVG2Bitmap($('svg', from)[0], img, { type: "image/" + (val=='jpg'?'jpeg':val), quality: 1.00, scale: 2.00, onloadIMG: whenLoaded	})
					append = $(document.createElement('div')).css('text-align', 'center')
						.append(img).append($(document.createElement('div')).append(telecharger));
					break;
				case 'svg':
					var svg = document.createElement('svg');
					var div = $(document.createElement('div')).attr({title: alt, id: val + id}).append(svg);
					var telecharger = $(document.createElement('a')).attr({href: "#", download: alt }).text('Télécharger')
					var whenLoaded = function(svg) { 
						var svgData = (new XMLSerializer()).serializeToString(svg);
						telecharger.attr('href', 'data:image/svg+xml;charset=utf8,' + encodeURIComponent(svgData));
					}
					SVG2Bitmap($('svg', from)[0], svg, { onloadIMG: whenLoaded })
					append = $(document.createElement('div')).css('text-align', 'center')
						.append(div).append($(document.createElement('div')).append(telecharger));
					break;
/*
				case 'png': // obsolete
				// case 'gif': // idem png en fait
				case 'jpg': // obsolete
				case 'jpeg': // obsolete
					var img = new Image;
					$(img).attr({alt: alt, id: val + id})
					var telecharger = $(document.createElement('a')).attr({href: "#", download: alt }).text('Télécharger')
					var whenLoaded = function(img) { telecharger.attr('href', img.src); }
					verovioSetIMG(img, $('svg', from)[0], val.toUpperCase(), { ratio: 1, quality: 1, onloadIMG: whenLoaded });
					append = $(document.createElement('div')).css('text-align', 'center')
						.append(img).append($(document.createElement('div')).append(telecharger));
					break;
*/
				default:
			}
			if(data || append) {
				var thisClass = 'spip_cadre ascore_code ascore_code-' + verovioId + ' ascore_code_' + val;
				var thisId = 'verovioCode' + id + '-' + val + '-' + verovioId;
				var isDataXML = data.isXML();
				if(isDataXML) thisClass += ' xml';
				if(val == "source") {
					var thisRel = inputFormat ? inputFormat.toUpperCase() : (isDataXML ? 'XML' : '');
					if(thisRel) thisRel += ' (source)';
						else thisRel = 'SOURCE';
				} else {
					var thisRel = val.toUpperCase();
					if(isDataXML) thisRel += ' (xml)'
				}
				var pre = $(document.createElement('pre'))
						.attr({rel: thisRel, id: thisId, class: thisClass});
				pre.append($(document.createElement('code')).text(data));
				if(append) $('code', pre).append(append);
				$(from).append(pre);
			}
		});
	}
	$('div:empty', from).remove();
	var source = $(from).data('source');
	/* apres 2 secondes, envoyer l'image SVG au serveur pour mise en cache */
	if(xOptions.xCache && source) {
		var urlAction = $(from).data('action');
		setTimeout(function(){
			var format = inputFormat ? inputFormat : (inline.isXML() ? 'xml' : '');
			$(from).removeAttr("data-action data-inline data-options data-xoptions");
			$.post(urlAction, { source: source, cmd: 'putASCORE', inline: btoa(inline), format: format, image: btoa(from.outerHTML) })
				.done(function() { console.log('Image SVG "' + attrId + '" transférée au serveur :-)');	})
				.fail(function() { console.log('Echec du transfert de l\'image SVG "' + attrId + '" au serveur :-(');	});
		}, 2000*verovioId);
		var format = 'PNG';
		var whenLoaded = function(img) {
			var id = format + ' "' + attrId + '" ' + img.src.length + 'o';
			$.post(urlAction, { source: source,	cmd: 'put' + format, data: img.src })
				.done(function() { console.log('Image ' + id + ' transférée au serveur :-)');	})
				.fail(function() { console.log('Echec du transfert de l\'image ' + id + ' au serveur :-(');	})
		}
		/* et encore apres 1 seconde, envoyer l'image PNG */
		setTimeout(function(){
			SVG2Bitmap($('svg', from)[0], new Image, { type: "image/" + format.toLowerCase(), onloadIMG: whenLoaded	})
		}, 2000*verovioId + 1000);
		/* // ancienne routine
		if(!browser.mozilla) setTimeout(function(){
			verovioSetIMG(false, $('svg', from)[0], 'PNG', {url:urlAction, key:source, id:attrId});
		}, 1000*verovioId);
		*/
	}
}

// Construction de toutes les nouvelles partitions
// En prealable : verifier s'il y a un cache sur le serveur
function verovioCreateAll() {
	$(".modele_ascore").each(function() {
		var from = this;
		if($(from).hasClass('working')) return;
		$(from).addClass('working');
		var source = $(from).data('source');
		var url = $(from).data('action');
		if(/var_mode=(re)?calcul/.test(window.location.search)) {
			verovioCreate(from);
			$(from).removeClass('working');
		} else
			$.ajax({
				url: url,
				method: "POST",
				data: { source: source, cmd: 'getASCORE' },
				dataType: "html"
			})
			.done(function(html) {
				if(html && html.trim().length) {
					// un cache existe
					console.log('Score en cache : ' + source);
					$(from).before(html);
					$(from).remove();
				} else
					verovioCreate(from);
			})
			.fail(function() {
				// on lance Verovio
				verovioCreate(from);
			})
			.always(function() {
				$(from).removeClass('working');
			});
	});
}

/*
// format : en majuscule ('PNG', 'JPG', 'SVG')
// opt : {url:, key:, id:, ratio:, quality:}
// quality : seulement pour les JPG
function verovioSetIMG(img, svg, format, opt) {
	var isSVG = format == 'SVG';
	var svgSize = svg.getBoundingClientRect();
	var svgXml = new XMLSerializer().serializeToString(svg);
	var ratio = (!isSVG || opt.ratio) ? opt.ratio : 1;
	if(!img) img = new Image();
	if(!opt) opt = {};
	if(format=='JPG') format = 'JPEG';
	var mimeType = isSVG ? 'svg+xml' : format.toLowerCase();
	opt.quality = opt.quality ? parseFloat((opt.quality).toFixed(2)) : {};
	var blob = new Blob([svgXml], {type: 'image/svg+xml;charset=utf-8'});
	var domURL = window.URL || window.webkitURL || window;
	var url = domURL.createObjectURL(blob);
	if(isSVG) { img.src = url; return; }
	img.onload = function () {
		if($(img).hasClass('drawn')) return;
		$(img).addClass('drawn');
		var width = Math.round(svgSize.width), height = Math.round(svgSize.height);
		$(img).css({ width: width, height: height });
		var canvas = jQuery(document.createElement('canvas'))
			.attr({width: width*ratio, height: height*ratio})[0];
		var ctx = canvas.getContext('2d');
		ctx.scale(ratio, ratio);
		if(format == 'JPEG') {
			// change non-opaque pixels to white
			var imgData = ctx.getImageData(0, 0, width*ratio, height*ratio);
			var data = imgData.data;
			for(var i=0; i<data.length; i+=4){
				if(data[i+3]<255){ data[i]=255; data[i+1]=255; data[i+2]=255; data[i+3]=255; }
			}
			ctx.putImageData(imgData, 0, 0);
		}
		ctx.drawImage(img, 0, 0);
		var data = canvas.toDataURL('image/' + mimeType, opt.quality);
		if(opt.url && opt.key) {
			var id = format + ' "' + (opt.id?opt.id:'??') + '" ' + data.length + 'o';
			$.post(opt.url, {	source: opt.key, cmd: 'put' + format, 	data: data })
				.done(function() { console.log('Image ' + id + ' transférée au serveur :-)');	})
				.fail(function() { console.log('Echec du transfert de l\'image ' + id + ' au serveur :-(');	})
				.always(function() { });
		} else
			img.src = data;
		if(params.onloadIMG) params.onloadIMG();
	}
	img.src = url;
	domURL.revokeObjectURL(url);
}
*/

(function($){
  $.fn.filterDecal = function() { 
		return $(this).filter(function(index) { 
			var html = $(this).html();
			var match = html.match(/^\-?[\d\.]+(pc|px)([^"]+)/);
			if(!match) return false;
			$(this).html(html.replace(/^(\-?[\d\.]+)pc/, '$1%'))
			return true;
		}); 
	}
	String.prototype.isXML = function() {
		return this.substr(0, 5) == '<?xml';
	}
	Array.prototype.unique = function() {
		return this.filter(function (value, index, self) { 
			return self.indexOf(value) === index;
		});
	}
	$(document).ready(function(){
		verovioCreateAll();
		onAjaxLoad(verovioCreateAll); 
	});
})(jQuery);
